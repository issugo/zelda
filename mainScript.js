async function main() {
    let map = new Map();
    //map.load("http://localhost/zelda/map.json");
    map.load("https://zelda-maurin.fr/map.json");
    //map.preDisplay("map")
    //map.decorate("map");
    map.generate("map");

    let link = new Link();
    link.decorate("link");
    document.onkeydown = function(event) {
        if (event.key === link.arrowDown) {
            moveDown(link, map);
        }
        if (event.key === link.arrowLeft) {
            moveLeft(link, map);
        }
        if (event.key === link.arrowRight) {
            moveRight(link, map);
        }
        if (event.key === link.arrowUp) {
            moveUp(link, map)
        }
    }
    new Buttons("button-container").createButtons();
    document.getElementById("left").onclick = function() {moveLeft(link, map)};
    document.getElementById("right").onclick = function() {moveRight(link, map)};
    document.getElementById("down").onclick = function() {moveDown(link, map)};
    document.getElementById("up").onclick = function() {moveUp(link, map)};
}

function moveDown(personnage, carte) {
    let nextY = personnage.calcPos("down");
    if(nextY > 6) {
        personnage.changeDir("down");
        carte.previousZone = carte.zonesActuelle["id"];
        carte.zonesActuelle = carte.searchZone(carte.zonesActuelle["id"] + 16);
        carte.decorate();
        personnage.teleport("up");
    } else {
        if(carte.usables.includes(carte.zonesActuelle["carte"][nextY + 4][personnage.x + 8])) {
            personnage.moveDown();
        } else {
            personnage.changeDir("down");
        }
    }
}

function moveLeft(personnage, carte) {
    let nextX = personnage.calcPos("left");
    if(nextX < -8) {
        personnage.changeDir("left");
        carte.previousZone = carte.zonesActuelle["id"];
        carte.zonesActuelle = carte.searchZone(carte.zonesActuelle["id"] - 1);
        carte.decorate();
        personnage.teleport("right");
    } else {
        if(carte.usables.includes(carte.zonesActuelle["carte"][personnage.y + 4][nextX + 8])) {
            personnage.moveLeft();
        } else {
            personnage.changeDir("left");
        }
    }
}

function moveRight(personnage, carte) {
    let nextX = personnage.calcPos("right");
    if(nextX > 7) {
        personnage.changeDir("right");
        carte.previousZone = carte.zonesActuelle["id"];
        carte.zonesActuelle = carte.searchZone(carte.zonesActuelle["id"] + 1)
        carte.decorate();
        personnage.teleport("left");
    } else {
        if(carte.usables.includes(carte.zonesActuelle["carte"][personnage.y + 4][nextX + 8])) {
            personnage.moveRight();
        } else {
            personnage.changeDir("right");
        }
    }
}

function moveUp(personnage, carte) {
    let nextY = personnage.calcPos("up");
    if(nextY < -4) {
        personnage.changeDir("up");
        carte.previousZone = carte.zonesActuelle["id"];
        carte.zonesActuelle = carte.searchZone(carte.zonesActuelle["id"] - 16);
        carte.decorate();
        personnage.teleport("down");
    } else {
        if(carte.usables.includes(carte.zonesActuelle["carte"][nextY + 4][personnage.x + 8])) {
            personnage.moveUp();
        } else {
            personnage.changeDir("up");
        }
    }
}

main();