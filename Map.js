function httpRequest(address, reqType, asyncProc) {
    let req = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    if (asyncProc) {
        req.onreadystatechange = function() {
            if (this.readyState === 4) {
                asyncProc(this);
            }
        };
    }
    req.open(reqType, address, !(!asyncProc));
    req.send();
    return req;
}

class Map {
    constructor() {
        this.map = null;
        this.zones = null;
        this.tiles = null;
        this.zoneDepart = null;
        this.zonesActuelle = null;
        this.previousZone = null;
        this.usables = [];
        //this.usable = [0, 2, 6, 12, 14, 18, 22, 24, 30, 34, 58, 70, 75, 76, 77, 81, 82, 83, 87, 88, 89, 93, 94, 95, 99, 100, 101, 105, 106, 107, 111, 112, 113, 117, 118, 119, 123, 124, 125, 126, 127, 128, 129, 131, 132, 133, 134, 135, 137, 138, 139, 140, 141, 143];
    }
    load(lien) {
        let temp = this;
        let req = httpRequest(lien, "GET");
        this.map = JSON.parse(req.response).map;
        this.zones = JSON.parse(req.response).zones;
        this.zones.forEach(zone => {
            if(zone.hasOwnProperty("depart")) {
                if(zone["depart"] === true) {
                    temp.zoneDepart = zone["id"];
                }
            }
        })
        this.tiles = JSON.parse(req.response).tiles;
        this.tiles.forEach(tile => {
            if(tile.hasOwnProperty("usable")) {
                if(tile["usable"] === true) {
                    temp.usables.push(tile["id"]);
                }
            }
        })
        this.zonesActuelle = this.searchZone(this.zoneDepart);
    }
    generate(id) {
        this.startLoader();
        let container = document.getElementById(id);
        this.zones.forEach(zone => {
            let div = document.createElement("div");
            div.classList.add("map");
            div.setAttribute("id", "zone"+zone["id"]);
            zone["carte"].forEach(element => {
                element.forEach(tile => {
                    let imageToDisplay = this.searchTile(tile)["name"]
                    let img = document.createElement("img");
                    img.setAttribute("src", "images/overworld/" + imageToDisplay)
                    div.appendChild(img);
                })
            });
            if(zone["id"] !== this.zoneDepart) {
                div.classList.add("hidden");
            }
            container.appendChild(div);
        })
        this.stopLoader();
    }
    startLoader() {
        document.getElementById("loader").classList.remove("hidden");
    }
    stopLoader() {
        document.getElementById("loader").classList.add("hidden");
        document.getElementById("map").classList.remove("hidden");
        document.getElementById("link").classList.remove("hidden");
        document.getElementById("button-container").classList.remove("hidden");
    }
    decorate() {
        document.getElementById("zone" + this.previousZone).classList.add("hidden");
        document.getElementById("zone" + this.zonesActuelle["id"]).classList.remove("hidden");
    }
    searchZone(id) {
        let j = 0;
        for(let i=0;i<this.zones.length;i++) {
            if(this.zones[i].hasOwnProperty("id")) {
                if(this.zones[i]["id"] === id) {
                    j = i;
                    break;
                }
            }
        }
        return this.zones[j];
    }
    searchTile(id) {
        let j = 0;
        for(let i=0;i<this.tiles.length;i++) {
            if(this.tiles[i].hasOwnProperty("id")) {
                if(this.tiles[i]["id"] === id) {
                    j = i
                    break;
                }
            }
        }
        return this.tiles[j];
    }
}