class Buttons {
    constructor(container) {
        this.container = container;
    }

    createButtons() {
        this.createButtonUp();
        this.createButtonLeft();
        this.createButtonDown();
        this.createButtonRight();
    }

    createButtonLeft() {
        let container = document.getElementById(this.container);
        let button = document.createElement("button");
        button.classList.add("arrow-button");
        button.setAttribute("id","left");
        let span = document.createElement("span");
        span.classList.add("arrow-left");
        span.innerHTML = "Left";
        button.appendChild(span);
        container.appendChild(button);
    }

    createButtonDown() {
        let container = document.getElementById(this.container);
        let button = document.createElement("button");
        button.classList.add("arrow-button");
        button.setAttribute("id","down");
        let span = document.createElement("span");
        span.classList.add("arrow-down");
        span.innerHTML = "Down";
        button.appendChild(span);
        container.appendChild(button);
    }

    createButtonRight() {
        let container = document.getElementById(this.container);
        let button = document.createElement("button");
        button.classList.add("arrow-button");
        button.setAttribute("id","right");
        let span = document.createElement("span");
        span.classList.add("arrow-right");
        span.innerHTML = "Right";
        button.appendChild(span);
        container.appendChild(button);
    }

    createButtonUp() {
        let container = document.getElementById(this.container);
        let button = document.createElement("button");
        button.classList.add("arrow-button");
        button.setAttribute("id","up");
        let span = document.createElement("span");
        span.classList.add("arrow-up");
        span.innerHTML = "Up";
        button.appendChild(span);
        container.appendChild(button);
    }
}