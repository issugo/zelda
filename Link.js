class Link {
    constructor() {
        // element
        this.div                = null;
        this.img                = null;
        // images
        this.baseImage          = "images/link/";
        this.linkFaceGauche     = this.baseImage + "link-face-gauche.png";
        this.linkFaceDroite     = this.baseImage + "link-face-droite.png";
        this.linkGauche1        = this.baseImage + "link-gauche-1.png";
        this.linkGauche2        = this.baseImage + "link-gauche-2.png";
        this.linkDroite1        = this.baseImage + "link-droite-1.png";
        this.linkDroite2        = this.baseImage + "link-droite-2.png";
        this.linkDosGauche      = this.baseImage + "link-dos-gauche.png";
        this.linkDosDroite      = this.baseImage + "link-dos-droite.png";
        this.currentImage       = null;
        // pos
        this.y                  = 1;
        this.x                  = 0;
        this.direction          = 4; // 1= gauche, 2 = haut, 3 = droite, 4 = bas
        //touch
        this.arrowDown = null;
        this.arrowUp = null;
        this.arrowLeft = null;
        this.arrowRight = null;
        this.defineTouch("ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight")
    }

    defineTouch(up, down, left, right) {
        this.arrowDown = down;
        this.arrowUp = up;
        this.arrowLeft = left;
        this.arrowRight = right;
    }

    decorate(id) {
        let div = document.getElementById(id);
        let img = document.createElement("img");
        img.setAttribute("src", this.linkFaceGauche);
        img.setAttribute("id", "imageLink");
        div.appendChild(img);
        div.style.position = "absolute";
        div.style.left = "calc(50% + " + this.x*16 + "px)";
        div.style.top = "calc(50% + " + this.y*16 + "px - 24px)";
        //div.style.left = screen.width/2 - 128 + (16*8) + "px";
        //div.style.top = screen.height/2 - 127 + (16*5) + "px";
        //div.style.top = screen.height/2 - 64 + (16*5) + "px";
        this.img = img
        this.div = div;
        this.currentImage = this.linkFaceGauche;
    }

    calcPos(movement) {
        if(movement === "right") {
            return this.x + 1;
        }
        if(movement === "left") {
            return this.x - 1;
        }
        if(movement === "up") {
            return this.y - 1;
        }
        if(movement === "down") {
            return this.y + 1;
        }
    }

    changeDir(direction) {
        if (direction === "left") {
            this.img.setAttribute("src", this.linkGauche1);
            this.currentImage = this.linkGauche1;
            this.direction = 1;
        }
        if (direction === "right") {
            this.img.setAttribute("src", this.linkDroite1);
            this.currentImage = this.linkDroite1;
            this.direction = 3;
        }
        if (direction === "up") {
            this.img.setAttribute("src", this.linkDosGauche);
            this.currentImage = this.linkDosGauche;
            this.direction = 2;
        }
        if (direction === "down") {
            this.img.setAttribute("src", this.linkFaceGauche);
            this.currentImage = this.linkFaceGauche;
            this.direction = 4;
        }
    }

    teleport(direction) {
        if (direction === "left") {
            this.x = -8;
            this.div.style.left = "calc(50% + " + this.x*16 + "px)";
        }
        if (direction === "right") {
            this.x = 7;
            this.div.style.left = "calc(50% + " + this.x*16 + "px)";
        }
        if (direction === "up") {
            this.y = -4;
            this.div.style.top = "calc(50% + " + this.y*16 + "px - 24px)";
        }
        if (direction === "down") {
            this.y = 6;
            this.div.style.top = "calc(50% + " + this.y*16 + "px - 24px)";
        }
    }

    moveLeft() {
        if(this.currentImage === this.linkGauche1) {
            this.img.setAttribute("src", this.linkGauche2);
            this.currentImage = this.linkGauche2;
        } else {
            this.img.setAttribute("src", this.linkGauche1);
            this.currentImage = this.linkGauche1;
        }
        if(this.direction === 1) {
            this.x -= 1;
            this.div.style.left = "calc(50% + " + this.x*16 + "px)";
        }
        this.direction = 1;
    }
    moveRight() {
        if(this.currentImage === this.linkDroite1) {
            this.img.setAttribute("src", this.linkDroite2);
            this.currentImage = this.linkDroite2;
        } else {
            this.img.setAttribute("src", this.linkDroite1);
            this.currentImage = this.linkDroite1;
        }
        if(this.direction === 3) {
            this.x += 1;
            this.div.style.left = "calc(50% + " + this.x*16 + "px)";
        }
        this.direction = 3;
    }
    moveUp() {
        if(this.currentImage === this.linkDosGauche) {
            this.img.setAttribute("src", this.linkDosDroite);
            this.currentImage = this.linkDosDroite;
        } else {
            this.img.setAttribute("src", this.linkDosGauche);
            this.currentImage = this.linkDosGauche;
        }
        if(this.direction === 2) {
            this.y -= 1;
            this.div.style.top = "calc(50% + " + this.y*16 + "px - 24px)";
        }
        this.direction = 2;
    }
    moveDown() {
        if(this.currentImage === this.linkFaceGauche) {
            this.img.setAttribute("src", this.linkFaceDroite);
            this.currentImage = this.linkFaceDroite;
        } else {
            this.img.setAttribute("src", this.linkFaceGauche);
            this.currentImage = this.linkFaceGauche;
        }
        if(this.direction === 4) {
            this.y += 1;
            this.div.style.top = "calc(50% + " + this.y*16 + "px - 24px)";
        }
        this.direction = 4;
    }
}